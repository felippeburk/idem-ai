import logging


def post_setup(hub, ctx):
    """
    A post_setup contract ensures that a log translation handler can be added to any of the other loggers
    """
    # The user opted out of translated logs in config
    if not hub.OPT.pop_ml.translate_logs:
        return ctx.ret

    # TODO call the idempotent "init" of pop-ml

    # Get the root logger
    root = logging.getLogger()

    # TODO create a logging Handler or Formatter that translates all logs with pop-ml

    # TODO add this Handler/Formatter to the root logger
    (root, ...)
