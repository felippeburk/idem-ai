def post_func(hub, ctx):
    """
    pop_tree/tree/mod/func is what returns docstrings for functions
    The return from this function is in this format:

        { <func>:
            {
                "doc": "",
                "file": "",
                "start_line_number": 0,
                "end_line_number": 1,
                "parameters": {},
            }
        }
    """
    # TODO call the idempotent "init" of pop-ml

    # TODO extend pop-ml's tokenizer with a custom tokenizer for pop-tree
    #    But maybe it's not that complicated.
    #    At this point we have the function's parameters,
    #    maybe we just add them to a skip_tokens list that we pass to "translate()"
    hub.tree.ml.tokenizer()

    for func in ctx.ret:
        ret_doc = ctx.ret[func]["doc"]
        # TODO translate the doc using pop-ml
        translated = (ret_doc, ...)

        # Replace the doc from the ret with the translated doc
        ctx.ret[func]["doc"] = translated

    return ctx.ret


def post_types(hub, ctx):
    """
    pop_tree/tree/mod/types is what returns docstrings for classes/mods/subsystems
    The return from this function is in this format:


        { <class> :
            {
                "ref": "",
                "doc": "",
                "signature": "",
                "attributes": "",
                "functions": "",
                "variables": "",
                "file": "",
                "start_line_number": 0,
                "end_line_number": 1,
            }
        }

    """
    # TODO call the idempotent "init" of pop-ml

    # TODO extend pop-ml's tokenizer with a custom tokenizer for pop-tree
    hub.tree.tokenizer.init()

    "doc"
    for cls in ctx.ret:
        ret_doc = ctx.ret[cls]["doc"]
        # TODO translate the doc using pop-ml
        translated = (ret_doc, ...)

        # Replace the doc from the ret with the translated doc
        ctx.ret[cls]["doc"] = translated

    return ctx.ret


def post_parse(hub, ctx):
    """
    pop_tree/tree/mod/parse is what returns docstrings for pop LoadedMods
    The return from this function is in this format:

        {
            "ref": "",
            "doc": "",
            "file": "",
            "attributes":  "",
            "classes": "",
            "functions": "",
            "variables": "",
        }
    """
    # TODO call the idempotent "init" of pop-ml

    # TODO extend pop-ml's tokenizer with a custom tokenizer for pop-tree
    hub.tree.ml.tokenizer()

    ret_doc = ctx.ret["doc"]
    # TODO translate the doc using pop-ml
    translated = (ret_doc, ...)

    # Replace the doc from the ret with the translated doc
    ctx.ret["doc"] = translated

    return ctx.ret
