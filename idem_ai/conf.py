# https://pop.readthedocs.io/en/latest/tutorial/quickstart.html#adding-configuration-data
# TODO should there be a config switch to enable/disable translating each thing? i.e comments, logs, docs, output
#    Or should it be a global switch to translate EVERYTHING
CONFIG = {
    "translate_logs": {
        "default": False,
        "help": "Enable the translation of logs with pop-ml",
        "dyne": "pop_ml",
    }
}

SUBCOMMANDS = {}

CLI_CONFIG = {
    "translate_logs": {"action": "store_true", "dyne": "pop_ml"},
}

DYNE = {
    "exec": ["exec"],
    "log": ["log"],
    "output": ["output"],
    "states": ["states"],
    "tree": ["tree"],
}
