from typing import List


def translate(
    hub, ctx, text: List[str], source_lang: str = None, dest_lang: str = None
):
    """
    An exec module to take input text and translate

    """
    # Fallback to configured languages
    if source_lang is None:
        source_lang = hub.OPT.pop_ml.source_lang
    if dest_lang is None:
        source_lang = hub.OPT.pop_ml.dest_lang

    # TODO call the idempotent "init" of pop-ml

    # TODO translate the text using pop-ml
    translated = (text, ...)

    return {
        "result": True,
        "comment": None,
        "ret": translated,
    }
