def post(hub, ctx):
    """
    Translate the comments from state output
    """
    # If there is no "comment" in the output then return right away (i.e. for describe and is_pending)
    if "comment" not in ctx.ret:
        return ctx.ret

    # TODO call the idempotent "init" of pop-ml

    comments = ctx.ret["comment"]

    # TODO translate the comments using pop-ml
    translated = (comments, ...)

    # Replace the comments from the ret with the translated comments
    ctx.ret["comment"] = translated

    return ctx.ret
