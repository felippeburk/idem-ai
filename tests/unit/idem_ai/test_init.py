def test_cli(mock_hub, hub):
    mock_hub.idem_ai.init.cli = hub.idem_ai.init.cli
    mock_hub.idem_ai.init.cli()
    mock_hub.pop.config.load.assert_called_once_with(["idem_ai"], "idem_ai")
