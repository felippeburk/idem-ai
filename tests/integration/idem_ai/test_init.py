from unittest import mock


def test_cli(hub):
    with mock.patch("sys.argv", ["idem_ai"]):
        hub.idem_ai.init.cli()
